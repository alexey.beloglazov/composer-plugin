################################################################################
# Gitlab CI integration for Drupal 9+ projects.
#
# Start by simply including this file and then override this file as needed.
# See https://gitlab.com/drupalspoons/composer-plugin
#
################################################################################

# A snippet that gets is used by jobs via 'extend' keyword.
.show_environment_variables:
  before_script:
    - echo -e "\e[0Ksection_start:`date +%s`:show_env_vars[collapsed=true]\r\e[0KShow Environment Variables"
    - env
    - echo -e "\e[0Ksection_end:`date +%s`:show_env_vars\r\e[0K"

# Determines when a Pipeline gets created.
workflow:
  rules:
    # These 3 rules from https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Workflows/MergeRequest-Pipelines.gitlab-ci.yml
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # The last rule above blocks manual and scheduled pipelines on non-default branch. Allow those.
    - if: $CI_PIPELINE_SOURCE == "schedule"

# Override when you want a variation like alternate PHP version or Drupal version.
variables:
  # These are top level vars so they appear on the Run Pipeline form https://docs.gitlab.com/ee/ci/pipelines/#prefill-variables-in-manual-pipelines
  PHP_TAG:
    value: "7.4"
    description: The version of PHP to use for this Pipeline.
  DRUPAL_CORE_CONSTRAINT:
    value: ^9
    description: The constraint which specifies the drupal/core version that is used.
  COMPOSER: "composer.spoons.json"
  WEB_ROOT:
    value: web
    description: The web root directory.

# The Docker image used by all jobs by default. Override as desired.
default:
  image:
    name: wodby/php:$PHP_TAG-dev

composer:
  stage: build
  rules:
    - if: '$DCI_SKIP_COMPOSER == "1"'
      when: never
    - when: always
  variables:
    # Used by the bin/setup script.
    NONINTERACTIVE: 1
    # Ensure the correct PHP server port is populated in the Composer file.
    WEB_PORT: "9000"
  # Use artifacts to copy codebase to subsequent jobs.
  # See https://lorisleiva.com/laravel-deployment-using-gitlab-pipelines/.
  artifacts:
    expire_in: 1 week
    expose_as: 'web-vendor'
    when: always
    paths:
      - vendor/
      - $WEB_ROOT
      - .composer-plugin.env
      - composer.spoons.json
      - composer.spoons.lock
  extends: .show_environment_variables
  script:
    # Symlinks the project into Drupal codebase, builds composer.spoons.json, and `composer install`.
    # Override with you own script if your needs are vastly unusual.
    - echo -e "\e[0Ksection_start:`date +%s`:my_2_section[collapsed=true]\r\e[0KInstall Dependencies"
    - bash <(curl -s https://gitlab.com/drupalspoons/composer-plugin/-/raw/master/bin/setup)
    - echo -e "\e[0Ksection_end:`date +%s`:my_2_section\r\e[0K"

validate:
  stage: test
  rules:
    - if: '$DCI_SKIP_VALIDATE == "1"'
      when: never
    - when: always
  script:
    # See default definitions at https://gitlab.com/drupalspoons/composer-plugin/-/blob/2.10.12/src/Handler.php#L161-172
    # Override in your project's composer.json
    - composer validate
    - composer lint

phpcs:
  stage: test
  rules:
    - if: '$DCI_SKIP_PHPCS == "1"'
      when: never
    - when: always
  script:
    - cp -u vendor/drupalspoons/composer-plugin/templates/phpcs.xml.dist $WEB_ROOT/modules/custom
    - composer phpcs -- --report-junit=junit.xml --report-full --report-summary --report-source
  allow_failure: true
  artifacts:
    expose_as: junit
    expire_in: 6 mos
    paths:
      - junit.xml
    reports:
      junit: junit.xml

stylelint:
  stage: test
  rules:
    - if: '$DCI_SKIP_STYLELINT == "1"'
      when: never
    - when: always
  script:
    # Installs all core javascript dependencies.
    - yarn --cwd $WEB_ROOT/core add stylelint-junit-formatter
    - composer stylelint -- --color || true
    - composer stylelint -- --color --custom-formatter node_modules/stylelint-junit-formatter > junit.xml || true
  allow_failure: true
  artifacts:
    expose_as: junit
    expire_in: 6 mos
    paths:
      - junit.xml
    reports:
      junit: junit.xml

eslint:
  stage: test
  rules:
    - if: '$DCI_SKIP_ESLINT == "1"'
      when: never
    #@todo Change this to 'always' once this job works.
    - when: never
  extends: .show_environment_variables
  script:
    - yarn --verbose --cwd $WEB_ROOT/core install
    - composer eslint -- --format junit -o junit.xml
  # Ignore exit code because https://github.com/eslint/eslint/issues/2949
  allow_failure: true
  artifacts:
    expose_as: junit
    expire_in: 6 mos
    paths:
      - junit.xml
    reports:
      junit: junit.xml

# A hidden re-usable job. Useful when using a job matrix.
# For example https://gitlab.com/drupalspoons/devel/-/blob/5.x/.gitlab-ci.yml
.phpunit-base:
  stage: test
  rules:
    - if: '$DCI_SKIP_PHPUNIT == "1"'
      when: never
    - when: always
  image:
    name: wodby/php:$PHP_TAG-dev
    # Do not start php-fpm as its port will be used by built-in PHP web server.
    entrypoint: ["sh", "-c"]
  variables:
    # https://docs.gitlab.com/runner/configuration/feature-flags.html#available-feature-flags
    FF_NETWORK_PER_BUILD: 1
    # The parent job container is available for services at http://build:9000.
    SIMPLETEST_BASE_URL: http://build:9000
    DB_DRIVER: mysql
    MYSQL_ROOT_PASSWORD: root
    MYSQL_DATABASE: db
    MYSQL_USER: db
    MYSQL_PASSWORD: db
    MARIADB_TAG: "10.3"
    POSTGRES_TAG: "10.5"
    POSTGRES_PASSWORD: db
    POSTGRES_DB: db
    POSTGRES_USER: db
    SELENIUM_CHROME_TAG: latest
    MINK_DRIVER_ARGS_WEBDRIVER: '["chrome", {"browserName":"chrome","chromeOptions":{"w3c":false,"args":["--disable-gpu","--headless", "--no-sandbox", "--disable-dev-shm-usage"]}}, "http://chrome:4444/wd/hub"]'
    BROWSERTEST_OUTPUT_DIRECTORY: /tmp
    # https://gitlab.com/gitlab-org/gitlab/-/issues/36373
    BROWSERTEST_OUTPUT_BASE_URL: ${CI_JOB_URL}/artifacts/file/${WEB_ROOT}
  services:
    - name: wodby/mariadb:$MARIADB_TAG
      alias: mariadb
    - name: wodby/postgres:$POSTGRES_TAG
      alias: postgres
    - name: selenium/standalone-chrome:$SELENIUM_CHROME_TAG
      alias: chrome
  script:
    #  Determine DB driver.
    - |
      [[ $DB_DRIVER == "sqlite" ]] && export SIMPLETEST_DB=sqlite://localhost/sites/default/files/.sqlite
      [[ $DB_DRIVER == "mysql" ]] && export SIMPLETEST_DB=mysql://$MYSQL_USER:$MYSQL_PASSWORD@mariadb/$MYSQL_DATABASE
      [[ $DB_DRIVER == "pgsql" ]] && export SIMPLETEST_DB=pgsql://$POSTGRES_USER:$POSTGRES_PASSWORD@postgres/$POSTGRES_DB
    - echo -e "\e[0Ksection_start:`date +%s`:my_first_section[collapsed=true]\r\e[0KShow+Export Environment Variables"
    - export
    - echo -e "\e[0Ksection_end:`date +%s`:my_first_section\r\e[0K"
    - mkdir -p $WEB_ROOT/sites/simpletest/browser_output
    - composer webserver >> webserver.log 2>&1 &
    # Provide some context on the test run.
    - vendor/bin/drush status
    # Use the same URL http://build:9000 for local and browser tests.
    - echo '127.0.0.1 build' | sudo tee -a  /etc/hosts
    # Finally, execute tests.
    # Don't use --testdox as that suppresses browser_output files.
    - composer unit -- --log-junit junit.xml --verbose $DCI_PHPUNIT_EXTRA
  artifacts:
    expire_in: 6 mos
    expose_as: 'junit-browser_output-webserver_log'
    when: always
    reports:
      junit: junit.xml
    paths:
      - junit.xml
      - webserver.log
      - $WEB_ROOT/sites/simpletest/browser_output

phpunit:
  # An include of the code above, for easy reuse. See https://docs.gitlab.com/ee/ci/yaml/#extends.
  extends: .phpunit-base

